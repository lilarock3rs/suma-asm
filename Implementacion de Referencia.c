#define TAMANOPORDEFECTO 10
#include <stdlib.h>
#include <stdio.h>

struct BigInteger {
	struct BigInteger*	anterior;	//El anterior en la lista enlazada
	struct BigInteger*	siguiente;	//El siguiente en la lista enlazada
	int			tamano;		//El tamaño total del arreglo
	int			usado;		//El espacio usado actualmente
	int*			numeros;	//El arreglo de enteros
};

typedef struct BigInteger BigInteger;

BigInteger* crearBigInteger(int tamano) {
	BigInteger* resultado;
	resultado = (BigInteger*)malloc(sizeof(BigInteger));
	resultado->numeros = (int*)malloc(sizeof(int) * tamano);
	resultado->tamano = tamano;
	resultado->usado = 0;
	int i = 0;
	for(; i < tamano; i++)
		resultado->numeros[i] = 0;
	return resultado;
}

void imprimirFormaDeUso(char* detalles) {
	printf("%s (%s)", "bigInteger Number [bigInteger size]", detalles);
}

BigInteger* convertirABigInteger(char* cadena, int tamano) {
	int i = 0;
	int cifrasSignificativas = 0;
	int ignorarCeros = 1; //Ignorar ceros a la izquierda
	while(cadena[i] != '\0') {
		if(cadena[i] == '0' && ignorarCeros) {
			i++; //Este dígito es un cero a la izquierda, no es una cifra significativa
			continue;
		}
		if((cadena[i] >= 48 && cadena[i] <= 57) || cadena[i] == '-') {
			if(ignorarCeros)
				ignorarCeros = 0; //Ya tenemos un valor distinto de cero, ya pueden haber ceros en el número
			if(cadena[i] == '-' && (cifrasSignificativas > 0 || (cadena[i + 1] < 49 || cadena[i + 1] > 58))) {
				imprimirFormaDeUso("El menos solo puede estar al inicio del nÃºmero y seguido de un dÃ­gito mayor que 0");
				exit(4);
			}
			cifrasSignificativas++;
			i++;
		} else {
			imprimirFormaDeUso("Caracter afuera de los valores permitidos");
			exit(4);
		}
	}
	if(tamano <= 0)
		tamano = TAMANOPORDEFECTO;
	int cerosIgnorados = i - cifrasSignificativas;
	int division = cifrasSignificativas / tamano;
	int modulo = cifrasSignificativas % tamano;
	BigInteger* retorno = crearBigInteger(modulo);
	BigInteger* actual = retorno;
	int j = 0;
	for(; j < division; j++) {
		actual->siguiente = crearBigInteger(tamano);
		actual->siguiente->anterior = actual;
		actual = actual->siguiente;
	}
	if(modulo == 0)
		retorno = retorno->siguiente;
	actual = retorno;
	i = cerosIgnorados;
	while(cadena[i] != '\0') {
		char truco[3]; //No quiero tener que reinventar la rueda
		if(cadena[i] == '-') { //Número negativo, necesitamos pasarle el signo a atoi
			//Ya tenemos garantizado que este caso se va a dar solo una vez y al inicio del número
			truco[0] = cadena[i];
			truco[1] = cadena[++i];
			truco[2] = '\0';
		} else {
			truco[0] = cadena[i];
			truco[1] = '\0';
			truco[2] = '\0';
		}
		int temporal = 10; //Valor de control
		temporal = atoi(truco);
		if(temporal == 10) {
			imprimirFormaDeUso("Valor de control igual a 10");
			exit(4);
		}
		actual->numeros[actual->usado] = temporal; //Después de tantas verificaciones, por fin insertamos el numero
		actual->usado++;
		i++;
		if(actual->usado == actual->tamano)
			actual = actual->siguiente;
	}
	return retorno;
}

void imprimirBigInteger(BigInteger* objetivo) {
	int i = 0;
	for(; i < objetivo->tamano; i++)
		printf("%i", objetivo->numeros[i]);
	if(objetivo->siguiente) {
		printf("%s", ", ");
		imprimirBigInteger(objetivo->siguiente);
	} else
		printf("%s", ".\n");
}

int main(int argc, char** argv) {
	//El comando usado para ejecutar el programa es la primera entrada en argv
	/*if(argc <= 1 || argc > 4) {
		imprimirFormaDeUso();
		return 1;
	}*/
	//int tamano = 0;
	/*if(argc == 3) {
		tamano = atoi(argv[1]);
		if(tamano == 0) {
			imprimirFormaDeUso();
			return 2;
		}
	}*/
	//if(tamano == 0)
	//He quemado un valor de prueba porque los argumentos parecen estar mal en Eclipse CDT
	BigInteger* prueba = convertirABigInteger("00-12345678901234567890555555555555564", 0);
	imprimirBigInteger(prueba);
	return 0;
}
