.text
#Retorna la dirección base del nuevo BigInteger
#Descripción del BigInteger
#0  puntero al anterior de la lista
#4  puntero al siguiente de la lista
#8  tamaño del arreglo de enteros
#12 entradas del arreglo usadas
#16 puntero al arreglo de enteros
#Parámetros
#$a1: tamaño deseado para el BigInteger
crearBigInteger:
	addi		$a0, $zero, 0x00000014 	#20 bytes de memoria
	addi		$v0, $zero, 0x00000009 	#9, código para reservar memoria
	syscall
	sw		$zero, 0($v0) 		#Inicializar el puntero al anterior en ceros
	sw		$zero, 4($v0) 		#Inicializar el puntero al siguiente en ceros
	sw		$a1, 8($v0) 		#Colocar al tamaño al parámetro $a1
	sw		$zero, 12($v0) 		#Inicializar el tamaño usado a cero
	add		$t0, $v0, $zero 	#Backup de la dirección base
	addi		$t1, $zero, 0x00000004 	#Necesitamos cuatro veces el tamaño del BigInteger
	mult		$t1, $a1
	mflo		$a0
	addi		$v0, $zero, 0x00000009
	syscall 				#Memoria para el arreglo de enteros
	sw		$v0, 16($t0)		#Inicializar el puntero al arreglo con la dirección devuelta por syscall
	add		$v0, $zero, $t0		#Restaurar la dirección base al registro de devolución
	jr		$ra

#Convierte una cadena a una lista de BigIntegers
#a0 Puntero a la cadena
#a1 Tamaño deseado para los BigInteger generados
convertirABigInteger:
	add		$t0, $zero, $zero		#i
	add		$t1, $zero, $zero		#cifrasSignificativas
	addi		$t2, $zero, 0x00000001		#ignorarCeros
	add		$t3, $zero, $a0			#Copia puntero a la cadena
primerPaso:						#Verifica el tamaño del número y los ceros a la izquierda
	lw		$t4, 0($t3)			#Carga el carácter en la posición actual
	addi		$t5, $zero, 0x00000030		#Carga 48 (El código del carácter '0') a $t5 
	bne		$t4, $t5, verificacionNumero	#Si el carácter no es 0, no hace falta esta comprobación
	beq		$t2, $zero, verificacionNumero	#Si no ignoramos ceros saltar se debe ignorar este código
	addi		$t0, $t0, 0x00000001		#Aumenta el tamaño de la cadena, mas no las cifras significativas
	addi		$t3, $t3, 0x00000004		#Aumenta el puntero a la cadena
	j		primerPaso			#Vuelve a empezar el ciclo
	slti		$t6, $t4, 0x00000030		#Coloca $t6 a 1 si el carácter actual es menor que 48 ('0')
#Preguntar a Francisco como funciona slt! (0xFFFFFFFF o 0x00000001)
